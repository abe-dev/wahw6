package com.abedev.wahw6;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(JUnit4.class)
public class ExampleUnitTest {
    private static final String FLICKR_URL = "https://api.flickr.com/services/feeds/photos_public.gne?format=json";
    @Test
    public void addition_isCorrect() throws Exception {
        OkHttpClient okHttpClient = new OkHttpClient();
        Call call = okHttpClient.newCall(new Request.Builder().url(FLICKR_URL).build());
        Response response = call.execute();
        byte[] bytes = response.body().bytes();
        int offset;
        for (offset = 0; offset < bytes.length; offset++) {
            if (bytes[offset] == '(') {
                offset++;
                break;
            }
        }
        int resultLenght = bytes.length - offset - 1;
        if (resultLenght > 0) {
            String result = new String(bytes, offset, resultLenght);
        }
    }
}