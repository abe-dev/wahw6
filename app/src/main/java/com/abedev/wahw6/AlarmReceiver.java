package com.abedev.wahw6;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by home on 17.09.2016.
 */
public class AlarmReceiver extends WakefulBroadcastReceiver {
    private static final String TAG = AlarmReceiver.class.getSimpleName();
    public static int START_TIME = 2 * 3600;
    public static int WINDOW_LENGTH = 2 * 3600;



    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG,"onReceive");
        SyncAdapter.doSync();
        schedule(context);
    }

    private static PendingIntent getIntent(Context context) {
        Intent intent = new Intent(context,AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,1,intent,0);
        return pendingIntent;
    }

    public static void schedule(Context context) {
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setWindow(AlarmManager.ELAPSED_REALTIME_WAKEUP,getStartTime(),WINDOW_LENGTH*1000,AlarmReceiver.getIntent(context));
    }

    private static long getStartTime() {
        int seconds = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) * 3600
                + Calendar.getInstance().get(Calendar.MINUTE) * 60
                + Calendar.getInstance().get(Calendar.SECOND);
        int delay;
        if (seconds > START_TIME)
            delay = (START_TIME + 24*3600) - seconds;
        else
            delay = START_TIME - seconds;
        return SystemClock.elapsedRealtime() + delay*1000;
    }
}
