package com.abedev.wahw6;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlarmManager;
import android.app.Application;
import android.content.Context;

/**
 * Created by home on 17.09.2016.
 */
public class App extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        createAccount();
        AlarmReceiver.schedule(this);
    }

    private void createAccount() {
        AccountManager accountManager =
                (AccountManager) getSystemService(
                        ACCOUNT_SERVICE);

        Account newAccount = Authenticator.getAccount();
        accountManager.addAccountExplicitly(newAccount, null, null);
    }
}
