package com.abedev.wahw6;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.abedev.wahw6.Authenticator;

/**
 * Created by home on 16.09.2016.
 */
public class AuthenticatorService extends Service {
    private Authenticator mAuthenticator;
    @Override
    public void onCreate() {
        mAuthenticator = new Authenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
