
package com.abedev.wahw6.data;

import static com.abedev.wahw6.data.PhotosPersistenceContract.PhotoEntry;
import static com.abedev.wahw6.data.PhotosPersistenceContract.*;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;


/**
 * Created by home on 13.09.2016.
 */
public class PhotosProvider extends ContentProvider {
    private static final int PHOTO = 100;
    private static final int PHOTO_ITEM = 101;
    private static final int SYNC = 200;
    private static final UriMatcher uriMatcher = buildUriMatcher();
    private PhotosDbHelper dbHelper;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(CONTENT_AUTHORITY, PhotoEntry.TABLE_NAME,PHOTO);
        uriMatcher.addURI(CONTENT_AUTHORITY, PhotoEntry.TABLE_NAME + "/*",PHOTO_ITEM);
        uriMatcher.addURI(CONTENT_AUTHORITY, SyncEntry.TABLE_NAME,SYNC);
        return uriMatcher;
    }
    @Override
    public boolean onCreate() {
        dbHelper = new PhotosDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch (uriMatcher.match(uri)) {
            case SYNC:
                retCursor = dbHelper.getReadableDatabase().query(
                        SyncEntry.TABLE_NAME,
                        projection,
                        PhotoEntry.COLUMN_NAME_ID + " = 0",
                        new String[]{},
                        null,
                        null,
                        sortOrder
                );
                break;
            case PHOTO:
                retCursor = dbHelper.getReadableDatabase().query(
                        PhotoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case PHOTO_ITEM:
                String[] where = {uri.getLastPathSegment()};
                retCursor = dbHelper.getReadableDatabase().query(
                        PhotoEntry.TABLE_NAME,
                        projection,
                        PhotoEntry.COLUMN_NAME_ID + " = ?",
                        where,
                        null,
                        null,
                        sortOrder
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case SYNC:
                return CONTENT_SYNC_TYPE;
            case PHOTO:
                return CONTENT_PHOTO_TYPE;
            case PHOTO_ITEM:
                return CONTENT_PHOTO_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int match = uriMatcher.match(uri);
        switch (match) {
            case SYNC:
                throw new UnsupportedOperationException("Insert not supported for uri: " + uri);
            case PHOTO:
                if(db.insert(PhotoEntry.TABLE_NAME,null,values)==-1)
                    return null;
                else
                    return PhotosPersistenceContract.PhotoEntry.buildPlaceUriWithId(values.getAsString(PhotoEntry.COLUMN_NAME_ID));
            case PHOTO_ITEM:
                throw new UnsupportedOperationException("Insert not supported for uri: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int match = uriMatcher.match(uri);
        switch (match) {
            case SYNC:
                throw new UnsupportedOperationException("Delete not supported for uri: " + uri);
            case PHOTO:
                return db.delete(PhotoEntry.TABLE_NAME,selection,selectionArgs);
            case PHOTO_ITEM:
                return db.delete(PhotoEntry.TABLE_NAME,PhotoEntry.COLUMN_NAME_ID + "= ? ",new String[]{uri.getLastPathSegment()});
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int match = uriMatcher.match(uri);
        switch (match) {
            case SYNC:
                return db.update(SyncEntry.TABLE_NAME,values,SyncEntry.COLUMN_NAME_ID + "= 0",new String[]{});
            case PHOTO:
                return db.update(PhotoEntry.TABLE_NAME,values,selection,selectionArgs);
            case PHOTO_ITEM:
                return db.update(PhotoEntry.TABLE_NAME,values,PhotoEntry.COLUMN_NAME_ID + "= ? ",new String[]{uri.getLastPathSegment()});
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }
}
