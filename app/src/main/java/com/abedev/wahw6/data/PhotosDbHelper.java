package com.abedev.wahw6.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.abedev.wahw6.SyncService;

import static com.abedev.wahw6.data.PhotosPersistenceContract.PhotoEntry;
import static com.abedev.wahw6.data.PhotosPersistenceContract.SyncEntry;

/**
 * Created by home on 13.09.2016.
 */
public class PhotosDbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "photos.db";
    private static final int DB_VERSION = 1;
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";

    private static final String CREATE_PHOTOS_QUERY =
            "CREATE TABLE " + PhotoEntry.TABLE_NAME + "(" +
                    PhotoEntry.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    PhotoEntry.COLUMN_NAME_LINK + TEXT_TYPE + COMMA_SEP +
                    PhotoEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    PhotoEntry.COLUMN_NAME_PHOTO + TEXT_TYPE + COMMA_SEP +
                    PhotoEntry.COLUMN_NAME_DATE_PUBLISHED + TEXT_TYPE + COMMA_SEP +
                    PhotoEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE +
                    " )";

    private static final String CREATE_SYNC_QUERY =
            "CREATE TABLE " + SyncEntry.TABLE_NAME + "(" +
                    SyncEntry.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    SyncEntry.COLUMN_NAME_SYNC_TIME + TEXT_TYPE +
                    " )";

    public PhotosDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_PHOTOS_QUERY);
        sqLiteDatabase.execSQL(CREATE_SYNC_QUERY);
        ContentValues cv = new ContentValues();
        cv.put(SyncEntry.COLUMN_NAME_ID,0);
        cv.put(SyncEntry.COLUMN_NAME_SYNC_TIME,"");
        sqLiteDatabase.insert(SyncEntry.TABLE_NAME,null,cv);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
