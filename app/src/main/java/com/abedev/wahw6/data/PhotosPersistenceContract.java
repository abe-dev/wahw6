package com.abedev.wahw6.data;

import android.net.Uri;
import android.provider.BaseColumns;

import com.abedev.wahw6.BuildConfig;

/**
 * Created by home on 13.09.2016.
 */
public final class PhotosPersistenceContract {
    private PhotosPersistenceContract() {
        throw new UnsupportedOperationException();
    }

    public static final String CONTENT_AUTHORITY = BuildConfig.APPLICATION_ID;
    public static final String CONTENT_PHOTO_TYPE = "vnd.android.cursor.dir/"
            + CONTENT_AUTHORITY + "/" + PhotoEntry.TABLE_NAME;;
    public static final String CONTENT_SYNC_TYPE = "vnd.android.cursor.dir/"
            + CONTENT_AUTHORITY + "/" + SyncEntry.TABLE_NAME;;
    public static final String CONTENT_PHOTO_ITEM_TYPE = "vnd.android.cursor.item/"
            + CONTENT_AUTHORITY + "/" + PhotoEntry.TABLE_NAME;;
    private static final String CONTENT_SCHEME = "content://";
    public static final Uri BASE_CONTENT_URI = Uri.parse(CONTENT_SCHEME + CONTENT_AUTHORITY);
    private static final String SEPARATOR = "/";

    public static abstract class PhotoEntry implements BaseColumns {
        public static final String TABLE_NAME = "place";
        public static final String COLUMN_NAME_ID = "_id";
        public static final String COLUMN_NAME_LINK = "link";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_PHOTO = "photo";
        public static final String COLUMN_NAME_DATE_PUBLISHED = "published";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final Uri CONTENT_PHOTO_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TABLE_NAME).build();

        public static Uri buildPlaceUriWithId(String placeId) {
            return CONTENT_PHOTO_URI.buildUpon().appendPath(placeId).build();
        }

    }

    public static abstract class SyncEntry implements BaseColumns {
        public static final String COLUMN_NAME_ID = "_id";
        public static final String TABLE_NAME = "sync";
        public static final String COLUMN_NAME_SYNC_TIME = "SyncTime";
        public static final Uri CONTENT_SYNC_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TABLE_NAME).build();
    }
}
