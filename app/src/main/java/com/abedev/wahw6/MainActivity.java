package com.abedev.wahw6;

import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.abedev.wahw6.data.PhotosPersistenceContract;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{
    private static final int REQUEST_CODE_UPDATE = 1001;
    private static final String TAG = MainActivity.class.getSimpleName();
    CursorAdapter adapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list);
        adapter = createAdapter();
        listView.setAdapter(adapter);
        getSupportLoaderManager().initLoader(0,null,this);
        updateIfNeeded();

    }

    private void updateIfNeeded() {
        AsyncTask<ContentResolver,Void,Void> task = new AsyncTask<ContentResolver, Void, Void>() {
            @Override
            protected Void doInBackground(ContentResolver... params) {
                if (SyncAdapter.isSyncExpired(params[0]))
                    SyncAdapter.doSync();
                return null;
            }
        };
        task.execute(getContentResolver());
    }

    private CursorAdapter createAdapter() {
        String[] from = new String[]{PhotosPersistenceContract.PhotoEntry.COLUMN_NAME_LINK};
        int[] to = new int[] {android.R.id.text1};
        return new SimpleCursorAdapter(this,android.R.layout.simple_list_item_1,null,from,to,0);
    }

     @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_refresh:
                refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void refresh() {
        SyncAdapter.doSync();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = PhotosPersistenceContract.PhotoEntry.CONTENT_PHOTO_URI;
        String[] projection = new String[] {
                PhotosPersistenceContract.PhotoEntry.COLUMN_NAME_ID,
                PhotosPersistenceContract.PhotoEntry.COLUMN_NAME_LINK};
        CursorLoader cursorLoader = new CursorLoader(this, uri, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        loader.reset();
    }

}
