package com.abedev.wahw6;

import android.accounts.Account;
import android.app.job.JobScheduler;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;

import com.abedev.wahw6.data.PhotosPersistenceContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.abedev.wahw6.data.PhotosPersistenceContract.PhotoEntry.COLUMN_NAME_DATE_PUBLISHED;
import static com.abedev.wahw6.data.PhotosPersistenceContract.PhotoEntry.COLUMN_NAME_DESCRIPTION;
import static com.abedev.wahw6.data.PhotosPersistenceContract.PhotoEntry.COLUMN_NAME_ID;
import static com.abedev.wahw6.data.PhotosPersistenceContract.PhotoEntry.COLUMN_NAME_LINK;
import static com.abedev.wahw6.data.PhotosPersistenceContract.PhotoEntry.COLUMN_NAME_PHOTO;
import static com.abedev.wahw6.data.PhotosPersistenceContract.PhotoEntry.COLUMN_NAME_TITLE;
import static com.abedev.wahw6.data.PhotosPersistenceContract.PhotoEntry.CONTENT_PHOTO_URI;

/**
 * Created by home on 16.09.2016.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String FLICKR_URL = "https://api.flickr.com/services/feeds/photos_public.gne?format=json";
    private static final String TAG = SyncAdapter.class.getSimpleName();

    ContentResolver mContentResolver;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(
            Account account,
            Bundle extras,
            String authority,
            ContentProviderClient provider,
            SyncResult syncResult) {
        update();
    }

    private void update() {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        JSONArray items = null;
        try {
            items = getItems();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return;
        }
        for (int i = 0; i < items.length(); i++) {
            try {
                JSONObject item = items.getJSONObject(i);
                operations.add(createInsertOperation(item, i % 10 == 0));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        operations.add(createUpdateSyncStatOperation());
        try {
            mContentResolver.applyBatch(PhotosPersistenceContract.CONTENT_AUTHORITY, operations);
            mContentResolver.notifyChange(CONTENT_PHOTO_URI, null);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }

    }

    private ContentProviderOperation createUpdateSyncStatOperation() {
        return ContentProviderOperation.newUpdate(PhotosPersistenceContract.SyncEntry.CONTENT_SYNC_URI)
                .withValue(PhotosPersistenceContract.SyncEntry.COLUMN_NAME_SYNC_TIME, Calendar.getInstance().getTime().getTime())
                .build();
    }

    private ContentProviderOperation createInsertOperation(JSONObject item, boolean yieldAllowed) throws JSONException {
        String link = item.getString("link");
        return ContentProviderOperation.newInsert(CONTENT_PHOTO_URI)
                .withYieldAllowed(yieldAllowed)
                .withValue(COLUMN_NAME_ID, getIdFromLink(link))
                .withValue(COLUMN_NAME_LINK, link)
                .withValue(COLUMN_NAME_TITLE, item.getString("title"))
                .withValue(COLUMN_NAME_PHOTO, item.getJSONObject("media").getString("m"))
                .withValue(COLUMN_NAME_DATE_PUBLISHED, item.getString("published"))
                .withValue(COLUMN_NAME_DESCRIPTION, item.getString("description"))
                .build();
    }

    private String getIdFromLink(String link) {
        return Uri.parse(link).getLastPathSegment();
    }


    private JSONArray getItems() throws IOException, JSONException {
        OkHttpClient okHttpClient = new OkHttpClient();
        Call call = okHttpClient.newCall(new Request.Builder().url(FLICKR_URL).build());
        Response response = call.execute();
        return new JSONObject(readClearJson(response)).getJSONArray("items");
    }

    private String readClearJson(Response response) throws IOException {
        String original = response.body().string();
        int offset;
        for (offset = 0; offset < original.length(); offset++) {
            if (original.charAt(offset) == '(') {
                offset++;
                break;
            }
        }
        return original.substring(offset, original.length() - 1);
    }

    public static void doSync() {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(Authenticator.getAccount(), PhotosPersistenceContract.CONTENT_AUTHORITY, settingsBundle);

    }

    public static boolean isSyncExpired(ContentResolver contentResolver) {
        boolean result = true;
        Cursor cursor = null;
        try {
            cursor = contentResolver.query(PhotosPersistenceContract.SyncEntry.CONTENT_SYNC_URI, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                long lastUpdate = cursor.getLong(cursor.getColumnIndex(PhotosPersistenceContract.SyncEntry.COLUMN_NAME_SYNC_TIME));
                Calendar now = Calendar.getInstance();
                Calendar updated = Calendar.getInstance();
                updated.setTimeInMillis(lastUpdate);
                if ((now.getTime().getTime() - lastUpdate) < 24 * 3600 * 1000
                        && (updated.get(Calendar.HOUR_OF_DAY) * 3600
                        + updated.get(Calendar.MINUTE) * 60
                        + updated.get(Calendar.SECOND)
                )
                        > (AlarmReceiver.START_TIME + AlarmReceiver.WINDOW_LENGTH))
                    result = false;

            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return result;
    }
}
