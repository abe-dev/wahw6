package com.abedev.wahw6;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by home on 17.09.2016.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = connManager.getActiveNetworkInfo();

        if (network != null && network.getType() == ConnectivityManager.TYPE_WIFI && network.isConnected())
            if (SyncAdapter.isSyncExpired(context.getContentResolver()))
                SyncAdapter.doSync();

    }
}
